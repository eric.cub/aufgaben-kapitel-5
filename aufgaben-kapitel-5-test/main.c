#include <stdio.h>

int main() {
    int x, y;
    printf("Taschenrechner\n");
    printf("Geben Sie die 1. Zahl ein:\n");
    scanf("%d", &x);
    printf("Geben Sie die 2. Zahl ein:\n");
    scanf("%d", &y);
    printf("Die Summe ergibt %d:\n", x+y);
    return 0;
}
